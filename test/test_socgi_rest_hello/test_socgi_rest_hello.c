#include "hetao_rest.h"

#include "LOGC.h"

RESTSERVICEENTRY GET_hello ;
int GET_hello( struct RestServiceContext *ctx )
{
	char		http_body[ 1024 ] = "" ;
	int		http_body_len = 0 ;
	
	int		nret = 0 ;
	
	InfoLog( __FILE__ , __LINE__ , "GET_hello" );
	
	BUFSTRCAT( http_body , http_body_len , "hello test_socgi_rest_hello.socgi\n" ) ;
	nret = RESTFormatHttpResponse( ctx , http_body , http_body_len , NULL ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTFormatHttpResponse failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "RESTFormatHttpResponse ok" );
		return 0;
	}
}

static struct RestServiceConfig		g_RestServiceConfigArray[] = 
	{
		{ HTTP_METHOD_GET , "/hello.do" , GET_hello } ,
		{ "" , "" , NULL }
	} ;

INITHTTPAPPLICATION InitHttpApplication ;
int InitHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	InfoLog( __FILE__ , __LINE__ , "InitHttpApplication" );
	
	ctl = RESTCreateRestServiceControler( g_RestServiceConfigArray ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTCreateRestServiceControler failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "RESTCreateRestServiceControler ok" );
	}
	
	SOCGISetUserData( ctx , ctl );
	
	return HTTP_OK;
}

CALLHTTPAPPLICATION CallHttpApplication ;
int CallHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	int				nret = 0 ;
	
	InfoLog( __FILE__ , __LINE__ , "CallHttpApplication" );
	
	ctl = SOCGIGetUserData( ctx ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIGetUserData failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "SOCGIGetUserData ok" );
	}
	
	nret = RESTDispatchRestServiceControler( ctl , ctx ) ;
	if( nret < 0 )
	{
		FatalLog( __FILE__ , __LINE__ , "RESTDispatchRestServiceControler failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_FATAL;
	}
	else if( nret > 0 )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTDispatchRestServiceControler failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "RESTDispatchRestServiceControler ok" );
	}
	
	return HTTP_OK;
}

CLEANHTTPAPPLICATION CleanHttpApplication ;
int CleanHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	InfoLog( __FILE__ , __LINE__ , "CleanHttpApplication" );
	
	ctl = SOCGIGetUserData( ctx ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIGetUserData failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "SOCGIGetUserData ok" );
	}
	
	RESTDestroyRestServiceControler( ctl );
	
	return HTTP_OK;
}

